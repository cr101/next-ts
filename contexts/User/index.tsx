import { createContext, useContext, useEffect, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import { User } from '../../types/User';

export interface UserContextInterface {
  user: User | null;
  setUser: (user: User | null) => void;
}

const defaultValue = null as unknown as UserContextInterface;
const UserContext = createContext<UserContextInterface>(defaultValue);

interface Props {
  children: React.ReactNode;
}

const ME = gql`
  query me {
    me {
      email
    }
  }
`;

function UserProvider({ children }: Props): JSX.Element {
  const [user, setUser] = useState<User | null>(null);
  const { data, loading, error } = useQuery(ME);

  useEffect((): void => {
    if (!loading && !error) {
      setUser(data.me);
    }
  }, [data, loading, error]);

  const value = {
    user,
    setUser,
  };

  return (
    <UserContext.Provider value={value}>
      { children }
    </UserContext.Provider>
  );
}

export const useUser = (): UserContextInterface => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('UserCtx must be called in UserProvider');
  }
  return context;
};

export default UserProvider;
