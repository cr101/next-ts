import styled from 'styled-components';

import Footer from './Footer';
import Header from './Header';

interface Props {
  children: React.ReactNode;
}

const Layout = ({ children }: Props): JSX.Element => (
  <Container>
    <Header />

    <Content>
      { children }
    </Content>

    <Footer />
  </Container>
);

const Container = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
`;

const Content = styled.div`
  flex: 1 0 auto;
`;

export default Layout;
