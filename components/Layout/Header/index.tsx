import { useEffect } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

import { useUser } from '../../../contexts/User';

const LOGOUT = gql`
  mutation logout {
    logout {
      id
    }
  }
`;

const Header = (): JSX.Element => {
  const router = useRouter();
  const [logout, { error, loading, called }] = useMutation(LOGOUT);
  const { user, setUser } = useUser();
  const handleLogout = (): void => {
    logout();
  };

  useEffect((): void => {
    if (called && !loading && !error) {
      toast.success('Logged out');
      setUser(null);
      router.push('/');
    }
  }, [called, loading]);

  return (
    <Container>
      <Link href="/">
        <LogoContainer>
          Next.js + Ts template
        </LogoContainer>
      </Link>
      { user
        ? (
          <Links>
            <Link href="/">
              <LinkText onClick={handleLogout}>Logout</LinkText>
            </Link>
            <Link href="/profile">
              <LinkText>My Profile</LinkText>
            </Link>
          </Links>
        ) : (
          <Links>
            <Link href="/login">
              <LinkText>Login</LinkText>
            </Link>
            <Link href="/register">
              <LinkText>Register</LinkText>
            </Link>
          </Links>
        )
      }
    </Container>
  );
};

const LogoContainer = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 25px;
  cursor: pointer;
  margin-left: 10px;
`;

const LinkText = styled.a`
  font-size: 20px;
  margin: 10px;
  align-items: center;
`;

const Container = styled.header`
  display: flex;
  justify-content: space-between;
  background-color: #282c34;
  height: 150px;
  align-items: center;
  color: white;
`;

const Links = styled.div`
  cursor: pointer;
`;

export default Header;
