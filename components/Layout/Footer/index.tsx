import styled from 'styled-components';

const Footer = (): JSX.Element => (
  <Container>
    Very impressive footer
  </Container>
);

const Container = styled.footer`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #282c34;
  font-size: 17px;
  text-align: center;
  height: 100px;
  color: white;
`;

export default Footer;
