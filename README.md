# next-ts template

This repository is the template for a react webapp using the Next.js framework,
Typescript, and GraphQL.

## Summary

* Dependencies
* Deploy for development
* Deploy for production
* Overview


## Dependencies

Here are the versions this project is developed on:

```
$ node --version
v10.16.0

$ npm -g --version
6.10.3
```

While you are at the root of the repository, install the dependencies with:

```
$ npm i
```


## Deploy for development

Once the dependencies are installed, while at the root of the repository, run:

```
$ npm run dev
```

The website will now be available via the development server. The endpoint
URL will display on the terminal. ESLint errors will also be visible here.

**Instructions on how to connect to the backend are coming.**


## Deploy for production

First, you will need to create a Zeit account and to edit the now.json file with your project name.
Then you will have to migrate your domain name to Zeit's DNS and get your Zeit personal token. Typing
the following commands will:
- build the project, and deploy it to Zeit's cloud functions under a random domain name
- redirect your personal domain name to the previous deployment

```
$ now --token $ZEIT_TOKEN
$ now --token $ZEIT_TOKEN alias mySuperSite.com
```

_OR_

You can use the Zeit's GitHub/GitLab integration (which works really good!)

No matter how you chose to deploy your app, you will have to provide some env
variables to your application. To do so you'll have to use the `now` CLI and to
edit the `now.json` file.

For example, let's expose our graphql endpoint:
```bash
$ now secrets add api-base-url "https://prisma-ts.herokuapp.com/graphql"
```

*Note that you have to provide your complete graphql endpoint (here
`https://<domain>/graphql`*

Now, we can edit our `now.json` file to add our env:
```json
{
  // <...> the rest of your configuration (ie: app name, now version, etc)
  "build": {
    "env": {
      "API_BASE_URL": "@api-base-url"
    }
  }
}
```


## Overview

This front is based on [Next.js](https://nextjs.org/) so it is a
[React](https://fr.reactjs.org/) application. The code is typed thanks to
[Typescript](https://www.typescriptlang.org/).

The whole point is to query a [GraphQL](https://graphql.org/) endpoint
via [Apollo](https://www.apollographql.com/) and render a user interface
out of the retrieved data.
