import styled from 'styled-components';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import { useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

import { useUser } from '../contexts/User';
import AuthForm from '../components/AuthForm';
import Layout from '../components/Layout';

const REGISTER = gql`
    mutation register($email: String!, $password: String!) {
        register(email: $email, password: $password) {
            id
            email
        }
    }
`;

function Register(): JSX.Element {
  const router = useRouter();
  const { setUser } = useUser();
  const [login, { data, error, loading, called }] = useMutation(REGISTER, {
    onError: (e) => {
      if (e.graphQLErrors[0]?.extensions?.code === 'FORBIDDEN') {
        toast.error('User already exists');
      }
    } });

  useEffect((): void => {
    if (called && !loading && !error) {
      toast.success('Account successfully created');
      setUser({ email: data.register.email });
      router.push('/');
    }
  }, [called, loading]);

  return (
    <Layout>
      <Container>
        <h1>Register</h1>

        <FormContainer>
          <AuthForm
            onSubmit={(email, password): void => {
              login({ variables: { email, password } });
            }}
          />
        </FormContainer>

        <p>
          Already have an account? { ' ' }
          <Link href="/login">
            <a>Log in</a>
          </Link>
        </p>
      </Container>
    </Layout>
  );
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const FormContainer = styled.div`
  width: 150px;
  height: 150px;
  background-color: lightgrey;
  padding: 2em;
  border-radius: 8px;
`;

export default Register;
