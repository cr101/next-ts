import App from 'next/app';
import Head from 'next/head';
import React from 'react';
import { ToastContainer } from 'react-toastify';

import UserProvider from '../contexts/User';
import withApollo from '../hoc/withApollo';

import '../css/fonts.css';
import '../css/global.css';
import 'react-toastify/dist/ReactToastify.css';

class MyApp extends App {
  render = (): JSX.Element => {
    const { Component, pageProps } = this.props;

    return (
      <>
        <Head>
          <title>My Super App!</title>
        </Head>

        <UserProvider>
          <ToastContainer />

          <Component {...pageProps} />
        </UserProvider>
      </>
    );
  }
}

export default withApollo(MyApp);
