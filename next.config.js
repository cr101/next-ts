const withCSS = require('@zeit/next-css');

const nextConfig = {
  env: {
    API_BASE_URL: process.env.API_BASE_URL || 'http://localhost:4000/graphql',
  },
};

module.exports = withCSS(nextConfig);
